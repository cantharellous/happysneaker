<div class="row justify-content-md-center align-items-center fxg1">
  <div class="col col-lg-3">
    <div class="card rounded-0 p-3">
      <form id="auth" name="auth">
        <h2 class="py-3">Авторизация</h2>
        <div class="form-group">
          <label for="exampleInputEmail1">Email</label>
          <input type="email" class="form-control" name="email" aria-describedby="emailHelp" required autofocus>
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Пароль</label>
          <input type="password" class="form-control" name="pwd" required>
        </div>
        <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop" onclick="sendForm(event)">Авторизоваться</button>
      </form>
    </div>
    <br>
    <p class="text-center">Нет аккаунта? <a href="/reg">Зарегистрируйся</a></p>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Авторизация</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="response" class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <a href="/cart" class="btn btn-primary">В магазин</a>
      </div>
    </div>
  </div>
</div>

<script>
  let response = document.querySelector('#response');
  function sendForm(e){
    e.preventDefault();
    
    let xhr = new XMLHttpRequest();
    xhr.open('POST', './logic/authenticated/auth.php');
    const formData = new FormData(document.forms.auth);

    xhr.onreadystatechange = function(){
      if(xhr.readyState === 4 && xhr.status === 200){ 
        response.innerHTML = '<p>'+ xhr.responseText +'</p>';
      } 
    }
    xhr.send(formData);
  };
</script>