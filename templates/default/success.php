<div class="container-fluid">

  <div class="row row-cols-2">
    <div class="col-12 col-md-9 pt15" id="cart">
      <h1>Спасибо за покупку!</h1>
      <p>Оплата произведена успешно!</p>
    </div>
    <div class="col-12 col-md-3 pt15">
      <div class="card shadow-sm position-sticky p-3 border-0 rounded-0" style="top: 40px">
        <h4>С этим так же берут</h4>
        <? require_once './logic/get_additionally.php'; ?>
        <? require_once 'block/contact_block.php'; ?>
      </div>
    </div>
  </div>

</div>