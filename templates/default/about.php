<!-- страница о магазине -->


<div class="container-fluid fxg1">
  <div class="row justify-content-md-center">
    <div class="col-12 col-md-8 my-5">
      <div class="row row-cols-2">
        <div class="col-12 col-md-6 pt15">
          <h1>О Happysneaker</h1>
          
          <p class="text-muted">Наша миссия - это то, что заставляет нас делать все возможное для расширения человеческого потенциала. Мы делаем это, создавая инновационные спортивные инновации, делая наши продукты более устойчивыми, создавая креативную и разнообразную глобальную команду и оказывая положительное влияние на сообщества, в которых мы живем и работаем. Компания Happysneaker, базирующаяся на Марсе, Регион Cydonia, включает бренды Nike, Converse и Jordan.</p>
        </div>
        <div class="col-12 col-md-6 pt15">
          <video class="media-container__video" autoplay="" loop="" muted="" playsinline="" width="100%">
            <source data-src="./video/400274_ABOUTNIKE2019_4_original.mp4" type="video/mp4" src="./video/400274_ABOUTNIKE2019_4_original.mp4">
          </video>
        </div>
      </div>
    </div>
  </div>
  <div class="row justify-content-md-center">
    <div class="col-12 col-md-8 my-5">
      <? require_once 'block/contact_block.php' ?>
    </div> 
  </div> 
</div>