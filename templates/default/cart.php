  <?
    $m_shop = '1058542864';
    $m_orderid = '1';
    $m_amount = number_format($price, 2, '.', '');
    $m_curr = 'RUB';
    $m_desc = base64_encode('Test');
    $m_key = 'kRpZVtpWyqdK1lsn';

    $arHash = array(
      $m_shop,
      $m_orderid,
      $m_amount,
      $m_curr,
      $m_desc
    );


    $arHash[] = $m_key;

    $sign = strtoupper(hash('sha256', implode(':', $arHash)));
  ?>
  <div class="container-fluid">

    <div class="row row-cols-2">
      <div class="col-12 col-md-9 pt15" id="cart">
        <h1>Корзина</h1>
        <ul class="list-group list-group-flush">
        </ul>
        <div>
          <div class="text-right" style="padding: 15px 0">
            <form method="post" action="https://payeer.com/merchant/">
              <input type="hidden" name="m_shop" value="<?=$m_shop?>">
              <input type="hidden" name="m_orderid" value="<?=$m_orderid?>">
              <input type="hidden" name="m_amount" id="m_amount" value="<?=$m_amount?>">
              <input type="hidden" name="m_curr" value="<?=$m_curr?>">
              <input type="hidden" name="m_desc" id="m_desc" value="<?=$m_desc?>">
              <input type="hidden" name="m_sign" id="m_sign" value="<?=$sign?>">
              <input type="submit" class="btn btn-dark" style="float: none" name="m_process" value="Оплатить" />
            </form>
          </div>
          <p class="text-right sum"></p>
        </div>
      </div>
      <div class="col-12 col-md-3 pt15">
        <div class="card shadow-sm position-sticky p-3 border-0 rounded-0" style="top: 40px">
          <h4>С этим так же берут</h4>
          <? require_once './logic/get_additionally.php'; ?>
          <? require_once 'block/contact_block.php'; ?>
        </div>
      </div>
    </div>

  </div>
