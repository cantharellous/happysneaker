<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Happysneaker</title>
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400,900&family=Open+Sans&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="../css/fa/font-awesome.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="../css/style.css">
  <script src="//code-ya.jivosite.com/widget/IryVYdMpVC" async></script>
</head>
<body class="bg-light">