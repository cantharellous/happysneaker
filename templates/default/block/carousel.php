<?php 
  # компонент слайдера

  
  # define - определяет именованную константу, наличие которой проверяется в файле config
  define('VG_ACCESS', true);
  # подключение файла config.php 
  # Оператор require_once() идентичен оператору require (), 
  # за исключением того, что PHP проверяет, был ли файл уже включен, 
  # и если да,то не включает его снова.
  require_once "./config/config.php";

  
  $result = $pdo->query('SELECT * FROM carousel');
  $rows = $result->fetchAll(PDO::FETCH_ASSOC);

?>
  <figure id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <!-- каждый слайд выводится с помощью цикла foreach -->
      <? foreach ($rows as $key => $value) { ?>
        <!-- если ключ массива равен 0, то элементу добавляется класс active -->
        <!-- чтобы слайдер понимал какой слайд первый, в ином случае возникнет ошибка -->
        <div class="carousel-item <? if($key == 0) echo 'active' ?>">
          <!-- изображение слайда -->
          <img src="./img/<?echo $value['img']?>" class="d-block w-100" alt="">
          <!-- блок описания слайда -->
          <figcaption class="carousel-description">
            <!-- заголовок  -->
            <p><?echo $value['title']?></p>
            <!-- подзаголовок -->
            <h1><?echo $value['subtitle']?></h1>
          </figcaption>
        </div>
        <!-- конец блока foreach -->
      <?}?>
    </div>
    <!-- кнопка слайда назад -->
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <!-- кнопка слайда вперед -->
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </figure>