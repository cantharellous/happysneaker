<?
  define('VG_ACCESS', true);
  require_once "./config/config.php";

  if(!empty($banner_id)){
    $sql = 'SELECT * FROM banners WHERE id = :id';
    $stmt = $pdo->prepare($sql);
    $stmt->execute([':id' => $banner_id]);
    $row = $stmt->fetch(PDO::FETCH_OBJ);
  }

?>
<? if(!empty($row)): ?>
  <div class="container-fluid banner">
    <div class="banner-item">
      <img src="/img/<?echo $row->img; ?>" class="d-block w-100" alt="">
      <figcaption class="carousel-description">
        <p> <? echo $row->title; ?> </p>
        <h1><?echo $row->description; ?></h1>
      </figcaption>
    </div>
  </div>
<? endif; ?>
