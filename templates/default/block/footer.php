
<div class="footer bg-dark text-white">
  <div class="container-fluid">
    <div class="row row-cols-3">
      <div class="col-12 col-md-4 pt15">

        <h1 class="navbar-brand text-white">
          Happysneaker
        </h1>
      </div>
      <div class="col-12 col-md-4 pt15">
        <div class="row">

          <ul class="footer-nav col-5">
            <li class="my-2"><h6>Меню: </h6></li>
            <li><a href="/" class="text-white-50">Главная</a></li>
            <li><a href="/about" class="text-white-50">О магазине</a></li>
            <li><a href="/delivery" class="text-white-50">Доставка</a></li>
          </ul>
          <ul class="footer-nav col-5">
            <li class="my-2"><h6>Каталог: </h6></li>
            <li><a href="/catalog" class="text-white-50">Весь каталог</a></li>
            <li><a href="/man" class="text-white-50">Мужская</a></li>
            <li><a href="/woman" class="text-white-50">Женская</a></li>
            <li><a href="/child" class="text-white-50">Детская</a></li>
          </ul>

        </div>
      </div>
      <div class="col-12 col-md-3 pt15 my-2">
        <p class="h-100"></p>

        <span class="align-bottom">© Happysneaker, <?echo date("Y");?> Все права защищены.</span>
      </div>
    </div>
    
  </div>

<div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-delay="9300">
  <div class="toast-header">
    
    <strong class="mr-auto"><i class="fa fa-shopping-cart" aria-hidden="true"></i>  Корзина</strong>
    <small class="text-muted">только что</small>
    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="toast-body">
    See? Just like this.
  </div>
</div>

<style>
  .footer .toast{
    bottom: -20px!important;
  }
  .footer .toast.show{
    bottom: 10px!important;
  }
  .toast-body{
    color: #000;
  }
</style>

<script>

  var count = 0;

  var cart_count = document.getElementById('cart-count');

  function pushCart(id, name, uid){

    var cart_toast = document.querySelector('.toast-body');

    id = encodeURIComponent(id);
    uid = encodeURIComponent(uid);

    let xhr = new XMLHttpRequest();

    xhr.open('POST', './logic/cart/set.php');

    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function(){

      if(xhr.readyState === 4 && xhr.status === 200){ 

        cart_toast.innerHTML = '<p>'+name+' добавлен в корзину</p>';
        $('.toast').toast('show');

        getCart(uid);
      } 
    }

    xhr.send('id='+id+'&uid='+uid);
  }

  function getCart(uid){

    uid = encodeURIComponent(uid);
    

    let xhr = new XMLHttpRequest();

    xhr.open('POST', './logic/cart/get.php');

    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function(){

      if(xhr.readyState === 4 && xhr.status === 200){ 

        let res = JSON.parse(xhr.responseText);
        count = res.length;

        cart_count.innerHTML = count;
        

        var sum = 0;

        let sumElement = document.querySelector('.sum');

        var productName = '';

        let list = document.querySelector('.list-group');

        list.innerHTML = '';

        for(i in res){

          let parent = document.createElement("div");

          parent.className += "list-group-item cart-item";

          parent.innerHTML = `
            <div class="cart-card">
              <img src="../img/boots/${res[i][0].img}" alt="">
              <div class="cart-name">
                <h4>${res[i][0].name}</h4> 
                <div class="cart-price">
                  <p>${Number(res[i][0].price)} ₽</p>
                </div>
              </div>
            </div>
            <div>
              <button type="button" onclick="deleteCart(${res[i][0].id})" class="btn btn-danger">
                <i class="fa fa-trash-o" aria-hidden="true"></i>
              </button>
            </div>
          `;

          list.append(parent);
          productName += res[i][0].name +',';
          sum += Number(res[i][0].price);
        }
        
        sumElement.innerHTML = 'Сумма: ' + sum;
        
      } 
      generateHash(productName, sum);
    }
    xhr.send('&uid='+uid);
  }

  function generateHash(info, price){


    let xhr = new XMLHttpRequest();

    xhr.open('POST', './logic/cart/generate_pay_hash.php');

    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function(){

      if(xhr.readyState === 4 && xhr.status === 200){ 
        const res = JSON.parse(xhr.responseText);
        var p = document.querySelector("#m_amount");
        var d = document.querySelector("#m_desc");
        var s = document.querySelector("#m_sign");
        p.setAttribute("value", res[0]); 
        d.setAttribute("value", res[1]); 
        s.setAttribute("value", res[2]); 
        console.log(res);
      } 
    }

    xhr.send('&info='+info+'&price='+price);
  }

  getCart(<?echo $_SESSION['user_id']?>);


  function deleteCart(id){

    var cart_toast = document.querySelector('.toast-body');

    id = encodeURIComponent(id);
    

    let xhr = new XMLHttpRequest();

    xhr.open('POST', './logic/cart/delete.php');

    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function(){

      if(xhr.readyState === 4 && xhr.status === 200){ 

        cart_toast.innerHTML = '<p>Товар удален</p>';

        $('.toast').toast('show');

        getCart(<?echo $_SESSION['user_id']?>);
      } 
    }

    xhr.send('id='+id+'&uid='+<?echo $_SESSION['user_id']?>);
  }
</script>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>