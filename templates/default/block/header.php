  <?
    session_start(); 

  ?>


  <nav class="navbar navbar-expand-lg navbar-light">

    <a class="navbar-brand" href="/"><strong>Happysneaker</strong></a> 

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">

      <ul class="navbar-nav m-auto">

        <li class="nav-item active">
          <a class="nav-link" href="/">Главная <span class="sr-only">(current)</span></a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="/about">О нас</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="/delivery">Доставка</a>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Категории
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/man">Мужская</a>
            <a class="dropdown-item" href="/woman">Женская</a>
            <a class="dropdown-item" href="/child">Детская</a>
          </div>
        </li>
      </ul>

      <? if( isset($_SESSION['user_email']) ): ?>
        <div class="form-inline my-2 my-lg-0">

          <p class="my-auto"><?echo $_SESSION['user_email'];?></p>
          <a href="/cart" type="button" class="btn">
            <i class="fa fa-shopping-cart" aria-hidden="true"></i> 
            <span class="badge badge-danger" id="cart-count">1</span>
          </a>
        </div>

      <? else: ?>
        <div class="form-inline my-2 my-lg-0">
          <a href="/auth">Авторизация</a> <span> / </span> <a href="/reg">Регистрация</a>

        </div>
      <? endif; ?>
    </div>
  </nav>