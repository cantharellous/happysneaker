<?php
  defined('VG_ACCESS') or die('505 ERROR');
  const SITE_URL = 'https://m228066.eurodir.ru/';
  const PATH = '/';

  $HOST = 'localhost';
  $USER = 'root'; // root
  $PASS = ''; //
  $DRIVER  = 'mysql';
  $DB_NAME = 'market'; // market
  $CHARSET = 'utf8';
  $OPTIONS = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];

  try {
    $pdo = new PDO("$DRIVER:host=$HOST;dbname=$DB_NAME;charset=$CHARSET",$USER, $PASS, $OPTIONS);  
    
  } catch (PDOException $e) {
    die('Ошибка подключения');
  }
  
?>