<? 
  header('Content-Type: text/html; charset=utf-8');
  session_start();
  require_once "./templates/default/block/head.php"; 
  require_once "./templates/default/block/header.php"; 
  
  

  if($_SERVER['REQUEST_URI'] == '/'){
    $page = 'home';
  } else {
    $page = substr($_SERVER['REQUEST_URI'], 1);
  }
  if( file_exists("./templates/default/$page.php") ) {
    require_once "./templates/default/$page.php";
  } else {
    echo '404';
  }
  require_once "./templates/default/block/footer.php";
?>
  