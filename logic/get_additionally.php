<?php 
  define('VG_ACCESS', true);
  require_once "./config/config.php";

  
  $sql = 'SELECT * FROM catalog WHERE additionally = :additionally';
  $stmt = $pdo->prepare($sql);
  $stmt->execute([':additionally' => 1]);

  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>

  <? foreach ($rows as $key => $value) { ?>
    <div class="card my-3" style="width: 100%;">
      <img src="./img/boots/<?echo $value['img']?>" class="card-img-top" alt="...">
      <div class="card-body">
        <h1 class="card-title h6"><?echo $value['name']?></h1>
        <p class="card-text"><?echo $value['price']?> ₽</p>
        <button class="btn btn-light" onclick="pushCart(<?echo $value['id']?>, `<?echo $value['name']?>`, <?echo $_SESSION['user_id'];?>)">
          <i class="fa fa-shopping-cart" aria-hidden="true"></i> 
          Добавить в корзину
        </button>
      </div>
    </div>
  <? } ?>