<?php
  define('VG_ACCESS', true);
  require_once '../../config/config.php';

  $pid = $_POST['id'];
  $uid = $_POST['uid'];
  $count_p = 1;

  if( !empty($pid) && !empty($uid) ){
    $sql = 'INSERT INTO cart(id_product, id_user, p_count) VALUES (:id_product, :id_user, 1)';
    $params = [':id_product' => $pid, ':id_user' => $uid];

    $stmt = $pdo->prepare($sql);
    $stmt->execute($params);
    
    echo 'Товар добавлен!';
  } else {
    echo 'Произошла ошибка при добавлении товара!';
  }