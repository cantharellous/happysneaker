<?
  define('VG_ACCESS', true);
  require_once '../../config/config.php';

  $uid = strip_tags(trim( $_POST['uid'] ));
  if( !empty($uid) ){
    $sql = 'SELECT * FROM cart WHERE id_user = :uid';
    $params = [':uid' => $uid];

    $stmt = $pdo->prepare($sql);
    $stmt->execute($params);

    $users = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $res = [];
    foreach ($users as $value) {
      $user = [];
      $sql = 'SELECT * FROM catalog WHERE id = :pid';
      $params = [':pid' => $value['id_product']];
  
      $stmt = $pdo->prepare($sql);
      $stmt->execute($params);
      $user = $stmt->fetchAll(PDO::FETCH_ASSOC);
      
      array_push($res, $user);
    }
    
    echo json_encode($res);
    

  } else {
    echo 'Произошла ошибка!';
  }


  