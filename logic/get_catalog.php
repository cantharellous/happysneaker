<?php 
  define('VG_ACCESS', true);
  require_once "./config/config.php";

  
  $sql = 'SELECT * FROM catalog';
  $stmt = $pdo->query($sql);

  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>

<div class="container-fluid">
  <h1>Каталог</h1>
  <div class="row row-cols-3">
    <? foreach ($rows as $key => $value) { ?>
      <div class="col-12 col-md-4 pt15 my-3">
        <div class="card border-0 rounded-0" style="width: 100%;">
          <img src="./img/boots/<?echo $value['img']?>" class="card-img-top" alt="<?echo $value['name']?>">
          <div class="card-body">
            <a href="/card?id=<?echo $value['id']?>" class="card-title"><?echo $value['name']?></a>
            <p class="card-text"><?echo $value['price']?> ₽</p>
            <button class="btn btn-light" onclick="pushCart(<?echo $value['id']?>, `<?echo $value['name']?>`, <?echo $_SESSION['user_id'];?>)">
              <i class="fa fa-shopping-cart" aria-hidden="true"></i> 
              Добавить в корзину
            </button>
            <!-- <a href="" class="btn btn-dark">
              <i class="fa fa-eye"></i> Смотреть
            </a> -->
          </div>
        </div>
      </div>
      
    <? } ?>
  </div>
</div>