<?php
  session_start(); 
  define('VG_ACCESS', true);
  require_once '../../config/config.php';

  $email = strip_tags(trim( $_POST['email'] ));
  $pwd = strip_tags(trim( $_POST['pwd'] ));
  
  if( !empty($email) && !empty($pwd) ){
    $sql = 'SELECT id, email, password FROM users WHERE email = :email';
    $params = [':email' => $email];

    $stmt = $pdo->prepare($sql);
    $stmt->execute($params);

    $user = $stmt->fetch(PDO::FETCH_OBJ);
    
    if($user){
      if(password_verify($pwd, $user->password)){
        $_SESSION['user_id'] = $user->id;
        $_SESSION['user_email'] = $user->email;
        echo 'Авторизация произведена успешно';
      }else{
        echo 'Неверный логин или пароль!';
      }
    } else {
      echo 'Неверный логин или пароль!';
    }
  } else {
    echo 'Пожалуйста заполните все поля!';
  }