<?php
  define('VG_ACCESS', true);
  require_once '../../config/config.php';

  $email = strip_tags(trim( $_POST['email'] ));
  $pwd = strip_tags(trim( $_POST['pwd'] ));
  $pwdC = strip_tags(trim( $_POST['pwdC'] ));

  if( !empty($email) && !empty($pwd) && !empty($pwdC) ){

    $sql_check = 'SELECT EXISTS(SELECT email FROM users WHERE email = :email)';
    $stmt_check = $pdo->prepare($sql_check);
    $stmt_check->execute([':email' => $email]);
    
    if( $stmt_check->fetchColumn() ){
      echo 'Пользователь с таким Email уже существует';
      die;
    }

    if($pwd == $pwdC){
      $pwd = password_hash($pwd, PASSWORD_DEFAULT);
      $sql = 'INSERT INTO users(email, password, access) VALUES (:email, :password, "user")';
      $params = [':email' => $email, ':password' => $pwd];

      $stmt = $pdo->prepare($sql);
      $stmt->execute($params);
      
      echo 'Вы успешно зарегистрировались! Спасибо за регистрацию!';
    } else {
      echo 'Пароли не совпадают!';
    }
  } else {
    echo 'Пожалуйста заполните все поля!';
  }