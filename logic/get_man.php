<?php 
  define('VG_ACCESS', true);
  require_once "./config/config.php";

  
  $sql = 'SELECT * FROM catalog WHERE category = :mark';
  $stmt = $pdo->prepare($sql);
  $stmt->execute([':mark' => 'м']);

  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>

<div class="container-fluid">
  <h1>Мужчинам</h1>
  <div class="row row-cols-3">
    <? foreach ($rows as $key => $value) { ?>
      <div class="col-12 col-md-4 pt15 my-3">
        <div class="card" style="width: 100%;">
          <img src="./img/boots/<?echo $value['img']?>" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title"><?echo $value['name']?></h5>
            <p class="card-text"><?echo $value['price']?> ₽</p>
            <button class="btn btn-light">
              <i class="fa fa-shopping-cart" aria-hidden="true"></i> 
              Добавить в корзину
            </button>
            <!-- <a href="" class="btn btn-dark">
              <i class="fa fa-eye"></i> Смотреть
            </a> -->
          </div>
        </div>
      </div>
      
    <? } ?>
  </div>
</div>